#pragma once
/*!
 * \file preprocessor_macro.h
 * 
 * プリプロセッサー・マクロ、あるいはプリプロセッサー・関数を定義しています。
 */

#ifdef __cplusplus
#   define    SY_EXTERN_C_BEGIN      extern "C" {
#   define    SY_EXTERN_C_END        }
#else
#   define    SY_EXTERN_C_BEGIN 
#   define    SY_EXTERN_C_END
#endif  /* def __cplusplus */

#define    SY_API

/*!
 * \def SY_ABS(_x_)
 *
 * _x_ の絶対値を返します。
 */
#define    SY_ABS(_x_)         (((_x_) > 0)     ? (_x_) : -(_x_))

/*!
 * \def SY_MIN(_x_, _y_)
 *
 * _x_ と _y_ の最小値を返します。
 */
#define    SY_MIN(_x_, _y_)    (((_x_) > (_y_)) ? (_x_) : (_y_))

/*!
 * \def SY_MAX(_x_, _y_)
 *
 * _x_ と _y_ の最大値を返します。
 */
#define    SY_MAX(_x_, _y_)    (((_x_) < (_y_)) ? (_x_) : (_y_))

/*!
 * \def SY_STRINGIFY(_x_)
 *
 * _x_ を C 文字列へ変換します。
 */
#define    SY_STRINGIFY(_x_)    #_x_
