#include "syrup.h"

int main()
{
    SYObject *that;

    that = SY_tuple_new( 3 );

    SY_tuple_put( that, 0, SY_long_from_int64( 1L ) );
    SY_tuple_put( that, 1, SY_long_from_int64( 2L ) );
    SY_tuple_put( that, 2, SY_str_from_string( "three" ) );

    /*
     * どーやってタプルを開放するかはまだ不明ｗ
     */

    return 0;
}
