#pragma once
/*!
 * \file object.h
 *
 * Syrup のオブジェクトの定義をしています。
 */

SY_EXTERN_C_BEGIN

#if defined(SY_DEBUG) && !defined(SY_TRACE_REFS)
#   define    SY_TRACE_REFS
#endif  /* defined(SY_DEBUG) && !defined(SY_TRACE_REFS) */

#ifdef SY_TRACE_REFS
#   define    _SYOBJECT_HEAD_EXTRA           \
    struct sy_object    *next_;              \
    struct sy_object    *prev_;

#   define    _SYOBJECT_EXTRA_INIT     0, 0
#else
#   define    _SYOBJECT_HEAD_EXTRA
#   define    _SYOBJECT_EXTRA_INIT

#endif  /* def SY_TRACE_REFS */

#define    SYOBJECT_HEAD                        \
    SYObject     base_;

#define    SYOBJECT_HEAD_INIT(_type_)           \
    { _SYOBJECT_EXTRA_INIT                      \
      1, _type_ },

#define    SYVAROBJECT_HEAD_INIT(_type_, _size_)    \
    { SYOBJECT_HEAD_INIT(_type_) size },

#define    SYOBJECT_VAR_HEAD                    \
    SYVarObject    base_

typedef struct sy_object {
    _SYOBJECT_HEAD_EXTRA

    SYssize_t               ref_count_;
    struct sy_type_object  *type_;
} SYObject;

#define SY_REF_COUNT(_that_)      (((SYObject *)(_that_))->ref_count_)
#define SY_TYPE(_that_)           (((SYObject *)(_that_))->type_)

typedef struct sy_var_object {
    SYOBJECT_HEAD

    SYssize_t                 size_;
} SYVarObject;

#define SY_SIZE(_that_)           (((SYVarObject *)(_that_))->size_)


typedef SYObject *     (*sy_unary_fn)(SYObject *);
typedef SYObject *     (*sy_binary_fn)(SYObject *, SYObject *);
typedef SYObject *     (*sy_ternary_fn)(SYObject *, SYObject *, SYObject *);

typedef int32_t        (*sy_inquiry_fn)(SYObject *);
typedef SYssize_t      (*sy_len_fn)(SYObject *);

typedef SYObject *     (*sy_ssize_unary_fn)(SYObject *, SYssize_t);
typedef SYObject *     (*sy_ssize_binary_fn)(SYObject *, SYssize_t, SYssize_t);


/*!
 * 渡された that の参照カウントを 1 つ増やします。
 */
SY_API void          SY_remain(SYObject *that);

/*!
 * 渡された that の参照カウントを 1 つ減らし、0 になった場合開放します。
 */
SY_API void          SY_release(SYObject *that);

/*!
 * 渡された that の参照カウントを返します。
 */
SY_API SYref_count_t SY_ref_count(SYObject * const that);



SY_EXTERN_C_END
