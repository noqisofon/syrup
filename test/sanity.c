#include "test.h"
#include "syrup/syrup.h"

static void simple_assert_example(void **state) {
    SYObject   *that = *state;

    assert_int_equal( that->ref_count_, 1 );
}

static int setup(void **state) {
    SYObject   *that = malloc( sizeof( SYObject ) );

    if ( that == NULL ) {

        return -1;
    }

    that->ref_count_ = 1;
    *state = that;

    return 0;
}

static int teardown(void **state) {
    free( *state );

    return 0;
}

int main() {
    const struct CMUnitTest tests[] = {
        cmocka_unit_test( simple_assert_example ),
    };

    return  cmocka_run_group_tests( tests, setup, teardown );
}

