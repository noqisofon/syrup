#pragma once
/*!
 * \file compatible.h
 *
 * プラットフォーム、あるいはコンパイラの違いをある程度吸収し、なるべく同じようななんかをします。
 *
 */

#ifndef HAVE_LONG_LONG
#   define    HAVE_LONG_LONG    1
#endif  /* ndef HAVE_LONG_LONG */

#ifndef SY_LONG_LONG
#   define    SY_LONG_LONG      long long

#   define    SY_LLONG_MIN      LLONG_MIN
#   define    SY_LLONG_MAX      LLONG_MAX

#   define    SY_ULLONG_MAX     ULLONG_MAX
#endif  /* ndef SY_LONG_LONG */

#define    SY_INT32_T      int32_t
#define    SY_UINT32_T    uint32_t

#define    SY_INT64_T      int64_t
#define    SY_UINT64_T    uint64_t

#ifndef SY_LONG_BITS_IN_DIGIT
#   if SIZEOF_VOID_P >= 8
#       define    SY_LONG_BITS_IN_DIGIT    30
#   else
#       define    SY_LONG_BITS_IN_DIGIT    15
#   endif  /* SIZEOF_VOID_P >= 8 */
#endif  /* ndef SY_LONG_BITS_IN_DIGIT */


typedef    intptr_t      SYintptr_t;
typedef    uintptr_t     SYuintptr_t;

#ifdef HAVE_SSIZE_T
typedef    ssize_t       SYssize_t;
#elif SIZEOF_VOID_P == SIZEOF_SIZE_T
typedef    SYintptr_t    SYssize_t;
#else
#   error "Syrup needs a typedef for SYssize_t in compatible.h"
#endif  /* def HAVE_SSIZE_T */

#define    SIZEOF_SYHASH_CODE_T    SIZEOF_SIZE_T
typedef    SYssize_t               SYhash_code_t;

#define    SIZEOF_SYUHASH_CODE_T   SIZEOF_SIZE_T
typedef    SYssize_t               SYuhash_code_t;

#ifdef SYSSIZE_T_CLEAN
typedef    SYssize_t               SYssize_clean_t;
#else
typedef    int                     SYssize_clean_t;
#endif  /* def SYSSIZE_T_CLEAN */

#define    SY_SIZE_MAX             SIZE_MAX

#define    SY_SSIZE_T_MAX          ((SYssize_t)(((size_t)-1) >> 1))

#define    SY_SSIZE_T_MIN          (-SY_SSIZE_T_MAX - 1)

/*!
 * 参照カウントの型です。
 */
typedef    SY_UINT32_T             SYref_count_t;

#ifndef SY_FORMAT_SIZE_T
#   if SIZEOF_SIZE_T == SIZEOF_INT && !defined(__APPLE__)
#       define    SY_FORMAT_SIZE_T    ""
#   elif SIZEOF_SIZE_T == SIZEOF_LONG
#       define    SY_FORMAT_SIZE_T    "l"
#   elif defined(MS_WINDOWS)
#       define    SY_FORMAT_SIZE_T    "I"
#   else
#       error "This platform's config.h needs to define SY_FORMAT_SIZE_T"
#   endif  /* SIZEOF_SIZE_T == SIZEOF_INT && !defined(__APPLE__) */
#endif  /* ndef SY_FORMAT_SIZE_T */

#if defined(_MSC_VER)

#   if defined(SY_LOCAL_AGGRESSIVE)
#       pragma optimize("agtw", on)
#   endif  /* defined(SY_LOCAL_AGGRESSIVE) */

#   pragma warning(disable: 4710)

#   define SY_LOCAL(_type_)            static          _type_ __fastcall
#   define SY_LOCAL_INLINE(_type_)     static __inline _type_ __fastcall

#else
#   define SY_LOCAL(_type_)            static          _type_
#   define SY_LOCAL_INLINE(_type_)     static   inline _type_

#endif  /* defined(_MSC_VER) */

#define   SY_MEMCPY    memcpy

#include <stdlib.h>

#ifdef HAVE_IEEEFP_H
#   include <ieeefp.h>
#endif  /* def HAVE_IEEEFP_H */

#include <math.h>

/**********************************************************************
 *
 * wrapper for <time.h> and/or <sys/time.h>
 *
 **********************************************************************/
#ifdef TIME_WITH_SYS_TIME
#   include <sys/time.h>
#   include <time.h>
#else
#    ifdef HAVE_SYS_TIME_H
#        include <sys/time.h>
#    else
#        include <time.h>
#    endif  /* def HAVE_SYS_TIME_H */
#endif  /* def TIME_WITH_SYS_TIME */

/**********************************************************************
 *
 * wrapper for <sys/select.h>
 *
 **********************************************************************/
#ifdef HAVE_SYS_SELECT_H
#   include <sys/select.h>
#endif  /* def HAVE_SYS_SELECT_H */

/**********************************************************************
 *
 * stat() and fstat() fiddling
 *
 **********************************************************************/
#ifdef HAVE_SYS_STAT_H
#   include <sys/stat.h>
#elif defined(HAVE_STAT_H)
#   include <stat.h>
#endif  /* def HAVE_SYS_STAT_H */

#ifndef S_IFMT
#   define    S_IFMT    0170000
#endif  /* ndef S_IFMT */
