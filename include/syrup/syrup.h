#pragma once

#include "syrup/config.h"

#include <stdio.h>
#ifdef HAVE_ERRNO_H
#   include <errno.h>
#endif  /* def HAVE_ERRNO_H */
#include <stdlib.h>
#ifdef HAVE_UNISTD_H
#   include <unistd.h>
#endif  /* def HAVE_UNISTD_H */
#ifdef HAVE_CRYPT_H
#   include <crypt.h>
#endif  /* def HAVE_CRYPT_H */

// size_t 用
#ifdef HAVE_STDDEF_H
#   include <stddef.h>
#endif  /* def HAVE_STDDEF_H */

// int32_t とか uint32_t とか用
#ifdef HAVE_STDINT_H
#   include <stdint.h>
#endif  /* def HAVE_STDINT_H */

#include "syrup/compatible.h"
#include "syrup/preprocessor_macro.h"




#include "syrup/object.h"


